package com.swagger.example.onlineshopping.service;

import com.swagger.example.onlineshopping.exception.ProductNotFoundException;
import com.swagger.example.onlineshopping.model.Product;
import com.swagger.example.onlineshopping.repo.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
class ProductServiceImplTest {
   @Autowired
   private ProductService service;
   @MockBean
   private ProductRepository repository;

   private List<Product> products;
   @BeforeEach
   public void startUp(){
    Product productA=new Product();
    productA.setId(10);
    productA.setName("Mac Book Pro");
    products= Collections.singletonList(productA);

    Mockito.when(repository.findAll()).thenReturn(products);
    //Mockito.when(repository.findById(productA.getId())).thenReturn(productA);
   }
    @Test
    void getProducts() {
    List<Product> result=service.getProducts();
     Assertions.assertEquals(products,result, "Products Should Match");
    }

    @Test
    void getById() {
    Product result=service.getById(products.get(0).getId());
    Assertions.assertEquals(products.get(0),result,"Product not found");
    }


 @Test
 void getByIdNotFound() {

  Assertions.assertThrows(ProductNotFoundException.class, ()->{
   Product result=service.getById(30);

 });}


    @Test
    void addProduct() {
    }

    @Test
    void remove() {
    }

    @Test
    void getProductsOfAPage() {
    }
}