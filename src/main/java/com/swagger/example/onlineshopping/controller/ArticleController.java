package com.swagger.example.onlineshopping.controller;

import com.swagger.example.onlineshopping.model.Article;
import com.swagger.example.onlineshopping.service.ArticleService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/online-shopping-article")
public class ArticleController {

    final ArticleService articleService;


    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping
    public List<Article> getAllArticles(){
        return articleService.getArticles();

    }

    @PostMapping
    public void addArticle(@RequestBody Article article){
         articleService.addArticle(article);


    }


    @DeleteMapping(value="/remove")
    public void removeArticle(@RequestParam("articleId") int articleId){
        articleService.remove(articleId);
    }

    @DeleteMapping(value="/removeAll")
    public void removeArticles(){
        articleService.remove();
    }


}
