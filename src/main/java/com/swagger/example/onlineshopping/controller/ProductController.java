package com.swagger.example.onlineshopping.controller;

import com.swagger.example.onlineshopping.model.Product;
import com.swagger.example.onlineshopping.service.ProductService;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/online-shopping")
@Api(description = "Product related operations")
public class ProductController {

    final ProductService productService;


    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> getAllProducts(){
        return productService.getProducts();

    }

    @GetMapping(value = "/search")
    public Product getProductById(@RequestParam("productId") int productId){
        return productService.getById(productId);

    }



    @PostMapping
    public ResponseEntity<Void> addProduct(@RequestBody Product product){
        int productId= productService.addProduct(product);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("productId created"+Integer.toString(productId))
                .build();

    }

    @DeleteMapping(value="/remove")
    public void removeProduct(@RequestParam("productId") int productId){
        productService.remove(productId);
    }

    @DeleteMapping(value="/removeAll")
    public void removeProducts(){
        productService.remove();
    }



    @GetMapping("/paginate")
    public List<Product> getProductsOfAPage(@RequestParam int pageNumber,
                                               @RequestParam int pageSize) {

        return productService.getProductsOfAPage(pageNumber, pageSize);
    }



}
