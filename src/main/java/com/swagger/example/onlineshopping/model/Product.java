package com.swagger.example.onlineshopping.model;

import lombok.*;

import javax.persistence.*;

@Getter
    @Setter
@NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @Entity
    @Table(name="product")
    public class Product {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="name")
    private String name;
    @OneToOne(targetEntity = Article.class, cascade = CascadeType.ALL)
    private Article article;

    /*public Product(String name,Article article){

        this.name=name;
        this.article=article;
    }*/

    }

