package com.swagger.example.onlineshopping.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="article")
public class Article {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="article_id")
    private int articleId;
    @Column
    private String dateCreated;
    @Column
    private String tagLine;
    @Column
    private String category;
    @Column
    private String content;
    @Column
    private String [] tags;

   /* @OneToOne(mappedBy = "article")
    private Product product;*/



}
