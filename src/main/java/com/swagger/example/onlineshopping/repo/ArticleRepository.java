package com.swagger.example.onlineshopping.repo;

import com.swagger.example.onlineshopping.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article,Integer> {

    void delete(Article article);



}
