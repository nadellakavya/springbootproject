package com.swagger.example.onlineshopping.repo;

import com.swagger.example.onlineshopping.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductRepository  extends JpaRepositoryImplementation<Product, Integer>,PagingAndSortingRepository<Product,Integer>   {

    List<Product> findAll();
    Product findById(int id);

    //Product save(Product product);

    Page<Product> findAll(Pageable pageable);

    void delete(Product product);


}
