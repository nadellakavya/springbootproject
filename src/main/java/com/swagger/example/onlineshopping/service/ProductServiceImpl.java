package com.swagger.example.onlineshopping.service;

import com.swagger.example.onlineshopping.exception.ProductNotFoundException;
import com.swagger.example.onlineshopping.model.Product;
import com.swagger.example.onlineshopping.repo.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService{
    final ProductRepository productRepository;
    @Override
    public List<Product> getProducts() {
        return (List<Product>)productRepository.findAll();
    }

    @Override
    public Product getById(int id) {

       Product product=productRepository.findById(id);
        if(product==null){
            throw new ProductNotFoundException("Product with:"+id+ " not found");
        }
        return product;
    }

    @Override
    public int addProduct(Product product) {
       // Product existing=new StudentEntity(student.getFirstName(),student.getLastName());

       productRepository.save(product);
        return product.getId();

    }

    @Override
    public void remove(int productId) {

        /*Optional<Product> existing=productRepository.findById(productId);
        if(!existing.isPresent()){
            throw new ProductNotFoundException("Product not found");
        }*/
        Product existing=productRepository.findById(productId);
        if(existing==null){
            throw new ProductNotFoundException("Product not found");
        }
        productRepository.delete(existing);

    }

    @Override
    public void remove() {
        productRepository.deleteAll();

    }





   //Pageable is an interface defined by Spring, which holds a PageRequest
    @Override
    public List<Product> getProductsOfAPage(int pageNumber, int pageSize) {
        Pageable paging = PageRequest.of(pageNumber, pageSize);
        //The Slice knows if it has content and if it is the first or last slice.
        // It is also capable of returning the Pageableused in the current and previous slices
        // Page is a sub-interface of Slice and has a couple of additional methods.
        // It knows the number of total pages in the table as well as the total number of records
      Page<Product> pagedResult = productRepository.findAll(paging);

        return pagedResult.toList();
    }



}
