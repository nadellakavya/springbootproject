package com.swagger.example.onlineshopping.service;

import com.swagger.example.onlineshopping.exception.ArticleNotFoundException;
import com.swagger.example.onlineshopping.model.Article;
import com.swagger.example.onlineshopping.repo.ArticleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ArticleServiceImpl implements ArticleService
{

    final ArticleRepository articleRepository;
    @Override
    public List<Article> getArticles() {
        return (List<Article>)articleRepository.findAll();
    }


    @Override
    public void addArticle(Article article) {


        articleRepository.save(article);


    }


    @Override
    public void remove(int articleId) {

        Optional<Article> existing=articleRepository.findById(articleId);
        if(!existing.isPresent()){
            throw new ArticleNotFoundException("Article not found");
        }

        articleRepository.delete(existing.get());

    }

    @Override
    public void remove() {
        articleRepository.deleteAll();

    }

}
