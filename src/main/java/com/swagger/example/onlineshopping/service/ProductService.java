package com.swagger.example.onlineshopping.service;

import com.swagger.example.onlineshopping.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> getProducts();
    Product getById(int id);

    int addProduct(Product product);

    void remove(int productId);
    void remove();
    List<Product> getProductsOfAPage(int pageNumber, int pageSize);

}
