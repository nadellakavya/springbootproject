package com.swagger.example.onlineshopping.service;

import com.swagger.example.onlineshopping.model.Article;

import java.util.List;

public interface ArticleService {

    List<Article> getArticles();
    void addArticle(Article article);

    void remove(int articleId);
    void remove();
}
